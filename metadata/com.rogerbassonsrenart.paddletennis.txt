Categories:Games
License:GPLv3
Web Site:
Source Code:https://github.com/rogertux/PaddleTennisAndroid
Issue Tracker:https://github.com/rogertux/PaddleTennisAndroid/issues

Auto Name:PaddleTennis
Summary:Pong clone
Description:
Pong clone, a tennis-like game.
.

Repo Type:git
Repo:https://github.com/rogertux/PaddleTennisAndroid

Build:1.0,1
    commit=779aeb64f2dfdde3714840c68d4e859ee5e3d41d
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

