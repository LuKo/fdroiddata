Categories:Office
License:GPLv3
Web Site:https://github.com/citiususc/calendula/blob/HEAD/readme.md
Source Code:https://github.com/citiususc/calendula
Issue Tracker:https://github.com/citiususc/calendula/issues

Auto Name:Calendula
Summary:Manage medical prescription
Description:
Manage your medical prescriptions through a simple and intuitive
interface. Users can easily define their own daily routines (wake
up, lunch, dinner, ...) and then link medication intakes to them.
.

Repo Type:git
Repo:https://github.com/citiususc/calendula

Build:1.0.3,4
    commit=332e05546ba909799042b3e6bd15e138a8f0b130
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Build:1.0.5,6
    commit=664903986e5e8b8c9cc1db31b84e71a6d639690a
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Build:1.1,7
    commit=8e8dac7b147759c39f6444606aaecf8973d8df42
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Build:1.1.1,8
    commit=v1.1.1
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Build:1.2,12
    commit=c5a9f25302b30ef18cca7d6916a865e5212e24cf
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' -e '/applicationVariants/,+6d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Build:1.2.1,13
    commit=5780169ccd4a64a75214b83ad4ef1b89580ee4d0
    subdir=Calendula
    gradle=yes
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=sed -i -e '/sonatype/d' -e '/michaelpardo/d' -e '/applicationVariants/,+6d' build.gradle && \
        cp -fR $$ActiveAndroid$$/src/com src/main/java

Maintainer Notes:
* Tags are unreliable
*
    srclibs=ActiveAndroid@08c6335cd7324c6e72da536b0c6fffa5a798f6a2
    prebuild=pushd $$ActiveAndroid$$ && \
        sed -i 's@<version>3.8.2</version>@<version>3.7.0</version>@g' pom.xml pom-child.xml tests/pom.xml && \
        $$MVN3$$ -Dmaven.test.skip=true install && popd && \
        sed -i -e '/sonatype/d' -e '/mavenCentral/amavenLocal()' -e 's/michaelpardo/activeandroid/g' -e 's/3.1.0-SNAPSHOT/3.1-SNAPSHOT/g'  build.gradle


.

Auto Update Mode:None
#Update Check Mode:Tags
Update Check Mode:RepoManifest
Current Version:1.2.1
Current Version Code:13

