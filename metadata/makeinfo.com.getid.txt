Categories:System
License:Unknown
Web Site:https://github.com/basil2style/getid/blob/HEAD/README.md
Source Code:https://github.com/basil2style/getid
Issue Tracker:https://github.com/basil2style/getid/issues

Auto Name:Get ID
Summary:Get device id and information
Description:
Get and show device details like:

* Device ID
* SIM serial number
* IMEI
* IMSI
* Google Service key
* WiFi mac address
.

Repo Type:git
Repo:https://github.com/basil2style/getid

Build:1.0,1
    disable=https://github.com/basil2style/getid/issues/1
    commit=5df9c2bb10aea49ec81080da08def524f1fda119
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

